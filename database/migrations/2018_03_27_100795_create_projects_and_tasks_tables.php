<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsAndTasksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('project_manager');
            $table->string('address1');
            $table->string('address2');
            $table->string('postcode');
            $table->string('state');
            $table->string('country');
            $table->date('start_date');
            $table->date('original_estimate');
            $table->date('current_estimate');
            $table->string('status')->default('unstarted');
            $table->timestamps();
        });

        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->default(0);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('elements', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->default(0);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('procurements', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('element_id')->unsigned()->default(0);
            $table->foreign('element_id')->references('id')->on('elements')->onDelete('cascade');
            $table->integer('location_id')->unsigned()->default(0);
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->integer('project_id')->unsigned()->default(0);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('order_number');
            $table->text('notes')->nullable();
            $table->date('start_date');
            $table->date('target_completion');
            $table->timestamps();
        });

        Schema::create('tasks', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('procurement_id')->unsigned()->default(0);
            $table->foreign('procurement_id')->references('id')->on('procurements')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->string('catergory');
            $table->string('supplier_contact');
            $table->date('start_date');
            $table->date('original_estimate');
            $table->date('actual_date')->nullable();
            $table->string('status')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('procurements');
        Schema::drop('elements');
        Schema::drop('tasks');
        Schema::drop('locations');
        Schema::drop('projects');
    }
}
