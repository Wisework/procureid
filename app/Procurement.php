<?php

namespace App;

use App\Project;
use App\Task;
use App\Location;
use App\Element;

use Illuminate\Database\Eloquent\Model;

class Procurement extends Model
{
	protected $table = 'procurements';

    protected $guarded = [];

	public function location()
	{
		return $this->belongsTo(Location::class);
	}
	public function element()
	{
		return $this->belongsTo(Element::class);
	}
	public function tasks()
	{
		return $this->hasMany(Task::class, 'procurement_id' , 'id');
	}
}
