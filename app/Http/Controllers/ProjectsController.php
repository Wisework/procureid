<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Procurement;
use App\Element;
use App\Project;
use App\Task;
use App\Location;
use Illuminate\Support\Facades\Input;
use Redirect;

class ProjectsController extends Controller
{
    public function index()
	{
		$projects = Project::all();
		return view('projects.index', compact('projects'));
	}

	public function create()
	{
		return view('projects.create');
	}

	public function store()
	{
		$input = Input::all();
		Project::create( $input );
 
		return Redirect::route('projects.index')->with('message', 'Project created');	}

	public function show(Project $project)
	{
		$projects = Project::all();
		return view('projects.show', compact('project','projects'));
	}

	public function edit(Project $project)
	{
		return view('projects.edit', compact('project'));
	}

	public function update(Project $project)
	{
		$input = array_except(Input::all(), '_method');
		$project->update($input);
 
		return Redirect::route('projects.show', $project->slug)->with('message', 'Project updated.');	}

	public function destroy(Project $project)
	{
		$project->delete();
 
		return Redirect::route('projects.index')->with('message', 'Project deleted.');
	}
}

