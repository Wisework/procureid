<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use Redirect;
use Auth;
use View;

class SystemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_users(Request $request)
    {
        $users = User::all();
        return View::make('system.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_users()
    {
        return View::make('system.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_users(Request $request)
    {
        $rolename = request('role');
        $role = Role::where('name', '=', $rolename)->first();
        

        $user = new User;

        $user->firstname = request('firstname');
        $user->lastname  = request('lastname');
        $user->email      = request('email');
        $user->phonenumber   = request('phonenumber');
        $user->company   = request('company');
        $user->password   = bcrypt(request('password'));

        

        $user->save();

        $user->assignRole($role);

        return Redirect::to('/system');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_users($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_users($id)
    {
        $user = User::find($id);
        return View::make('system.users.edit', [ 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_users(Request $request, $id)
    {

        $rolename = request('role');
        $role = Role::where('name', '=', $rolename)->first();

        $user = User::find($id);

        $user->firstname = request('firstname');
        $user->lastname  = request('lastname');
        $user->email      = request('email');
        $user->phonenumber   = request('phonenumber');
        $user->company   = request('company');
        $user->password   = bcrypt(request('password'));

        $user->save();

        User::find($id)->roles()->detach();
        $user->assignRole($role);

        return Redirect::to('/system');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_users($id)
    {
        User::find($id)->roles()->detach();
        User::destroy($id);
        return Redirect::to('/system');
    }
}
