<?php

namespace App;

use Illuminate\Http\Request;
use App\Element;
use App\Procurement;
use App\Project;
use App\Location;
use Illuminate\Database\Eloquent\Model;


class Task extends Model
{
	protected $table = 'tasks';

	protected $guarded = [];


    public function procurement()
	{
		return $this->belongsTo(Procurement::class);
	}
	public function project()
	{
		return $this->belongsTo(Project::class);
	}
}

