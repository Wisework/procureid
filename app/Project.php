<?php

namespace App;

use App\Element;
use App\Task;
use App\Location;
use App\Procurement;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $table = 'projects';

	protected $guarded = [];

	public function elements()
	{
		return $this->hasMany(Element::class, 'project_id' , 'id');
	}
	public function tasks()
	{
		return $this->hasMany(Task::class);
	}
	public function locations()
	{
		return $this->hasMany(Location::class, 'project_id' , 'id');
	}
}

