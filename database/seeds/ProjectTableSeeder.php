<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $projects = array(
            ['id' => 1, 
            'name' => 'Embassy Building A', 
            'slug' => 'embassy-a', 
            'project_manager' => 'Mr Project Manager',
            'address1' => '111 Elizabeth Street',
            'address2' => 'Melbourne',
            'postcode' => '3000',
            'state' => 'VIC',
            'country' => 'Australia',
            'start_date' => Carbon::parse('2018-01-02'),
            'original_estimate' => Carbon::parse('2018-03-02'),
            'current_estimate' => Carbon::parse('2018-04-02'),
            'status' => 'unstarted',
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 2, 
            'name' => 'Domain Park', 
            'slug' => 'domain-park', 
            'project_manager' => 'Justin Williams',
            'address1' => '111 Bourke Street',
            'address2' => 'Melbourne',
            'postcode' => '3000',
            'state' => 'VIC',
            'country' => 'Australia',
            'start_date' => Carbon::parse('2018-01-02'),
            'original_estimate' => Carbon::parse('2018-07-16'),
            'current_estimate' => Carbon::parse('2018-07-16'),
            'status' => 'unstarted',
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
        );
        DB::table('projects')->insert($projects);
    }
}
