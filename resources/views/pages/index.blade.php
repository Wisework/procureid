@extends('layouts.app')

@section('content')
   	<div class="completion-view">
   		<div class="row">
   			<div class="col-md-3 col-sm-3">
   				<div class="active-button dash-button">
   					<a href="">
   						<div class="l-half">
   							<p class="dash-number">4</p>
   						</div>
   						<div class="r-half">
   							<p class="dash-desc">Active <br>Projects</p>
   						</div>
   						<div class="full">
   							<p class="dash-full">Filter By Active</p>
   						</div>
   					</a>
   				</div>
   			</div>
   			<div class="col-md-3 col-sm-3">
   				<div class="overdue-button dash-button">
   					<a href="">
   						<div class="l-half">
   							<p class="dash-number">2</p>
   						</div>
   						<div class="r-half">
   							<p class="dash-desc">Overdue <br>Projects</p>
   						</div>
   						<div class="full">
   							<p class="dash-full">Filter By Overdue</p>
   						</div>
   					</a>
   				</div>
   			</div>
   			<div class="col-md-3 col-sm-3">
   				<div class="tasksoverdue-button dash-button">
   					<a href="">
   						<div class="l-half">
   							<p class="dash-number">40</p>
   						</div>
   						<div class="r-half">
   							<p class="dash-desc">Tasks <br>Overdue</p>
   						</div>
   						<div class="full">
   							<p class="dash-full">View Overdue Tasks</p>
   						</div>
   					</a>
   				</div>
   			</div>

   			<div class="col-md-3 col-sm-3">
   				<div class="configue-options">
   					<p><a href=""><img src="img/download-icon.png" class="download"></a><a href="" class="blue-btn btn"><span class="plus">+</span> Add Project</a></p>
   				</div>
   			</div>
   		</div>
   	</div>

   	<div class="search-filter">
   		<div class="row">
   			<div class="col-sm-6">
   				<form class="navbar-form" role="search">
    				<div class="input-group add-on">
      					<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
      					<div class="input-group-btn">
        					<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      					</div>
    				</div>
  				</form>
   			</div>
   			<div class="col-sm-2">
   				<p>Sort by:</p>
				<div class="dropdown">
  					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    				Due Date <span class="caret"></span>
  					</button>

 						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    						<li><a href="#">Due Date</a></li>
    						<li><a href="#">Another action</a></li>
    						<li><a href="#">Something else here</a></li>
  						</ul>
				</div>
   			</div>
   			<div class="col-sm-2">
   				<p>Filter by project status:</p>
				<div class="dropdown">
  					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    				All active projects <span class="caret"></span>
  					</button>

 						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    						<li><a href="#">All active projects</a></li>
    						<li><a href="#">Another action</a></li>
    						<li><a href="#">Something else here</a></li>
  						</ul>
				</div>
   			</div>
   			<div class="col-sm-2">
   				<p>Filter by project manager:</p>
				<div class="dropdown">
  					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    				My projects <span class="caret"></span>
  					</button>

 						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    						<li><a href="#">My projects</a></li>
    						<li><a href="#">Another action</a></li>
    						<li><a href="#">Something else here</a></li>
  						</ul>
				</div>
   			</div>
   		</div>
   	</div>

   	<div class="project-status">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="col-sm-4">
   					<div class="project-name">
   						<img src="img/project-icon.png" alt="">
   						<h3>Parkdale Heights Complex</h3>
   						<p>13 Smith Street, Parkdale Heights VIC</p>
   					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="pm-name">
   						<p>PM: Justin Williams</p>
   					</div>
   				</div>
   				<div class="col-sm-4">
   					<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   					<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   					<p class="above-project progress-current"><span>Current Est:</span>10 Apr 2017</p>
   					<div class="progress">
  						<div class="progress-bar progress-bar-success" style="width: 35%">
    						<span>45</span>
  						</div>
  						<div class="progress-bar progress-bar-warning" style="width: 55%">
    						<span>65</span>
  						</div>
  						<div class="progress-bar progress-bar-danger" style="width: 10%">
    						<span>12</span>
  						</div>
					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="project-controls">
   						<img src="img/overdue-icon.png" class="time-icon">
   						<img src="img/open-icon.png" >
   						<img src="img/more-icon.png" >
   					</div>
   				</div>
   			</div>
   		</div>
	</div>

	<div class="project-status">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="col-sm-4">
   					<div class="project-name">
   						<img src="img/project-icon.png" alt="">
   						<h3>Domain Park Highrise</h3>
   						<p>45 Albert Road, Albert Park, SA</p>
   					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="pm-name">
   						<p>PM: Justin Williams</p>
   					</div>
   				</div>
   				<div class="col-sm-4">
   					<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   					<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   					<p class="above-project progress-current"><span>Current Est:</span>10 Apr 2017</p>
   					<div class="progress">
  						<div class="progress-bar progress-bar-success" style="width: 40%">
    						<span>61</span>
  						</div>
  						<div class="progress-bar progress-bar-warning" style="width: 35%">
    						<span>55</span>
  						</div>
  						<div class="progress-bar progress-bar-danger" style="width: 25%">
    						<span>28</span>
  						</div>
					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="project-controls">
   						<img src="img/overdue-icon.png" class="time-icon">
   						<img src="img/open-icon.png">
   						<img src="img/more-icon.png">
   					</div>
   				</div>
   			</div>
   		</div>
	</div>

	<div class="project-status">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="col-sm-4">
   					<div class="project-name">
   						<img src="img/project-icon.png" alt="">
   						<h3>Melbourne Airport</h3>
   						<p>Melbourne Airport, Tullamarine VIC</p>
   					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="pm-name">
   						<p>PM: Justin Williams</p>
   					</div>
   				</div>
   				<div class="col-sm-4">
   					<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   					<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   					<div class="progress">
  						<div class="progress-bar progress-bar-success" style="width: 70%">
    						<span>250</span>
  						</div>
  						<div class="progress-bar progress-bar-warning" style="width: 30%">
    						<span>101</span>
  						</div>
					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="project-controls">
   						<img src="img/complete-icon.png" class="time-icon">
   						<img src="img/open-icon.png">
   						<img src="img/more-icon.png">
   					</div>
   				</div>
   			</div>
   		</div>
	</div>

	<div class="project-status">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="col-sm-4">
   					<div class="project-name">
   						<img src="img/project-icon.png" alt="">
   						<h3>Docklands Carpark</h3>
   						<p>787 Collins Street, Docklands VIC</p>
   					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="pm-name">
   						<p>PM: Justin Williams</p>
   					</div>
   				</div>
   				<div class="col-sm-4">
   					<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   					<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   					<div class="progress">
  						<div class="progress-bar progress-bar-success" style="width: 35%">
    						<span>54</span>
  						</div>
  						<div class="progress-bar progress-bar-warning" style="width: 65%">
    						<span>100</span>
  						</div>
					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="project-controls">
   						<img src="img/complete-icon.png" class="time-icon">
   						<img src="img/open-icon.png">
   						<img src="img/more-icon.png">
   					</div>
   				</div>
   			</div>
   		</div>
	</div>

	<div class="project-status">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="col-sm-4">
   					<div class="project-name">
   						<img src="img/project-icon.png" alt="">
   						<h3>Sydney Towers</h3>
   						<p>Crown Street, Sydney NSW</p>
   					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="pm-name">
   						<p>PM: Justin Williams</p>
   					</div>
   				</div>
   				<div class="col-sm-4">
					<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   					<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   					<div class="progress">
  						<div class="progress-bar progress-bar-success" style="width: 10%">
    						<span>2</span>
  						</div>
  						<div class="progress-bar progress-bar-warning" style="width: 90%">
    						<span>43</span>
  						</div>
					</div>
   				</div>
   				<div class="col-sm-2">
   					<div class="project-controls">
   						<img src="img/complete-icon.png" class="time-icon">
   						<img src="img/open-icon.png">
   						<img src="img/more-icon.png">
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
@endsection