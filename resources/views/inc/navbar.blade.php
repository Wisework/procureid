 <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="{{URL::asset('/img/logo.png')}}" alt=""></a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="/tasks">Tasks</a></li>
            @if(Auth::user()->hasRole('Company Admin'))
            <li><a href="/system">System Config</a></li>
            @endif 
          </ul>
          <div class="user-admin">
          	<ul>
          		<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        
                        {{ Auth::user()->firstname }} 
                        <img src="/uploads/avatars/{{ Auth::user()->avatar }}">
                    </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ url('/my-profile') }}"><i class="fa fa-btn fa-user"></i>Profile</a>
                </li>
                <li>
                    <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                </li>
            </ul>

                </li>
            </ul>
          </div>
        </div><!--/.nav-collapse -->

      </div>
</nav>