<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Redirect;
use Image;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(User $user)
    {
        $user = Auth::user();
        return view('users.my-profile', compact('user'));
    }

    public function edit(User $user)
    {   
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function update_avatar(Request $request){
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return back();
    }

    public function update(User $user)
    { 
        if(Auth::user()->email == request('email')) {
        
        $this->validate(request(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'company' => 'required',
          //  'email' => 'required|email|unique:users',
            'phonenumber' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->company = request('company');
        // $user->email = request('email');
        $user->phonenumber = request('phonenumber');
        $user->password = bcrypt(request('password'));

        $user->save();

        return Redirect::to('/my-profile');
    }
    else{
        
    $this->validate(request(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'company' => 'required',
            'email' => 'required|email|unique:users',
            'phonenumber' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->company = request('company');
        $user->email = request('email');
        $user->phonenumber = request('phonenumber');
        $user->password = bcrypt(request('password'));

        $user->save();

        return Redirect::to('/my-profile');
    }
    }
}