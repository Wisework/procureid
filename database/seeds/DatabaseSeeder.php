<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();
 
    $this->call(ProjectTableSeeder::class);
    $this->call(LocationsTableSeeder::class);
    $this->call(ElementsTableSeeder::class);
    $this->call(ProcurementsTableSeeder::class);
    $this->call(TasksTableSeeder::class);
    }
}
