<?php 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function() {

Route::get('/', 'PagesController@index');
Route::get('/project', 'PagesController@projects');
Route::get('/tasks', 'PagesController@tasks');
Route::get('/system-page', 'PagesController@system');
Route::get('/proj-config', 'PagesController@projectconfiguration');
Route::get('/add-procurement', 'PagesController@addprocurement');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/my-profile', 'UserController@profile');
Route::get('/my-profile/{user}',  ['as' => 'users.edit', 'uses' => 'UserController@edit']);
Route::patch('/my-profile/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);
Route::post('/my-profile', 'UserController@update_avatar');

/* Sytem Config Routes */
Route::group(['middleware' => 'is.admin'], function () {
Route::post('/system', ['as' => 'system.users.store', 'uses' => 'SystemController@store_users']);
Route::get('/system', ['as' => 'system.users.index', 'uses' => 'SystemController@index_users']);
Route::get('/system/users/create', ['as' => 'system.users.create', 'uses' => 'SystemController@create_users']);
Route::delete('/system/users/{user} ', ['as' => 'system.users.destroy', 'uses' => 'SystemController@destroy_users']);
Route::patch('/system/users/{user}', ['as' => 'system.users.update', 'uses' => 'SystemController@update_users']);
Route::get('/system/users/{user}/edit', ['as' => 'system.users.edit', 'uses' => 'SystemController@edit_users']);
});

/* Projects and Tasks Routes */
Route::model('tasks', 'Task');
Route::model('procurements', 'Procurement');
Route::model('elements', 'Element');
Route::model('locations', 'Location');
Route::model('projects', 'Project');

Route::bind('task', function($value, $route) {
	return App\Task::whereSlug($value)->first();
});
Route::bind('project', function($value, $route) {
	return App\Project::whereSlug($value)->first();
});
Route::resource('projects.procurements', 'ProcurementsController');
Route::resource('projects.locations', 'LocationsController');
Route::resource('projects.elements', 'ElementsController');
Route::resource('/projects', 'ProjectsController');
Route::resource('procurements.tasks', 'TasksController');



});


Auth::routes();


