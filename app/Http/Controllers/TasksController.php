<?php

namespace App\Http\Controllers;

use App\Procurement;
use App\Project;
use App\Location;
use App\Task;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function index(Procurement $procurement)
	{
		return view('tasks.index', compact('procurement'));
	}

	public function create(Procurement $procurement)
	{
		return view('tasks.create', compact('procurement'));
	}

	public function store(Project $project, Procurement $procurement)
	{
		$input = Input::all();
		$input['procurement_id'] = $procurement->id;
		Task::create( $input );
 
		return Redirect::route('projects.show', $project->slug)->with('message', 'Task created.');
	}

	public function show(Procurement $procurement, Project $project, Task $task)
	{
		return view('tasks.show', compact('project','procurement', 'task'));
	}

	public function edit(Procurement $procurement, Task $task)
	{
		return view('tasks.edit', compact('procurement', 'task'));
	}

	public function update(Project $project, Procurement $procurement, Task $task)
	{
		$input = array_except(Input::all(), '_method');
		$task->update($input);

		if (strtotime($task->actual_date) == null) {
			$task->status = null;
		}
		elseif (strtotime($task->actual_date)  > strtotime($task->original_estimate) ) {
			$task->status = 'Overdue';
		}
		elseif (strtotime($task->actual_date)  <= strtotime($task->original_estimate)) {
			$task->status = 'Complete';
		}

		$status = $task->status;
		$task->update(array('status' => $status));
		
 
		return Redirect::route('projects.show', $project->slug)->with('message', 'Task updated.');
	}

	public function destroy(Procurement $procurement, Task $task)
	{
		$task->delete();
 
		return Redirect::route('projects.show', $procurement->id)->with('message', 'Task deleted.');
	}
}

