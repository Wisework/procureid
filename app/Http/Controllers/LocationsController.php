<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Procurement;
use App\Task;
use App\Element;
use DB;
use App\Location;
use Illuminate\Support\Facades\Input;
use Redirect;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Location $location)
    {
        $procurements = Procurement::where('location_id','=', $location->id)
            ->get();

        $elements = Procurement::join('elements', 'procurements.element_id', '=', 'elements.id')
            ->where('procurements.location_id','=', $location->id)
            ->get();

        $tasks = Procurement::join('tasks', 'procurements.id', '=', 'tasks.procurement_id')
            ->where('procurements.location_id','=', $location->id)
            ->get();

        $projects = Project::all();

        return view('locations.show', compact('project','projects','location','locations','procurements','tasks','elements'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
