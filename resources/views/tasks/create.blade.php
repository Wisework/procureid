@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Task</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('procurements.tasks.store' , $procurement->id) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                            <label for="slug" class="col-md-4 control-label">Slug</label>

                            <div class="col-md-6">
                                <input id="slug" type="text" class="form-control" name="slug" value="{{ old('slug') }}" required autofocus>

                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('catergory') ? ' has-error' : '' }}">
                            <label for="catergory" class="col-md-4 control-label">Catergory</label>

                            <div class="col-md-6">
                                <select name="catergory" class="form-control">
                                    <option value="Design" name="Design" >Design</option>
                                    <option value="Production" name="Production">Production</option>
                                    <option value="Procurement Manager" name="Procurement Manager">Shipping</option>
                                </select>

                                @if ($errors->has('catergory'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('catergory') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('supplier_contact') ? ' has-error' : '' }}">
                            <label for="supplier_contact" class="col-md-4 control-label">Supplier Contact</label>

                            <div class="col-md-6">
                                <input id="supplier_contact" type="text" class="form-control" name="supplier_contact" value="{{ old('project_manager') }}" required autofocus>

                                @if ($errors->has('project_manager'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('project_manager') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                            <label for="start_date" class="col-md-4 control-label">Start Date</label>

                            <div class="col-md-6">
                                <input id="start_date" type="date" class="form-control" name="start_date" value="{{ old('start_date') }}" autofocus>

                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('original_estimate') ? ' has-error' : '' }}">
                            <label for="original_estimate" class="col-md-4 control-label">Original Estimate</label>

                            <div class="col-md-6">
                                <input id="original_estimate" type="date" class="form-control" name="original_estimate" value="{{ old('original_estimate') }}" autofocus>

                                @if ($errors->has('original_estimate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('original_estimate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create Task
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection