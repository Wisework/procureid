@extends('layouts.app')

@section('content')
   		<div class="new-procurement">
   			<div class="row">
   				<div class="col-sm-8">
   					<div class="row">
   						<div class="col-sm-6">
   							<div class="project-name">
   								<img src="img/project-icon.png" alt="">
   								<h3>Parkside Heights Complex</h3>
   								<p>13 Smith Street, Parkdale Heights Vic</p>
   							</div>
   						</div>
   						<div class="col-sm-6">
   							<div class="pm-name">
   								<p>Project Manager: <span>Justin Williams</span></p>
   							</div>
   						</div>
   					</div>
   					
   				</div>
   				<div class="col-sm-4">
   					
   				</div>
   			</div>
   		</div>

   		
   			<div class="row">
   				<div class="col-sm-5">
   					<div class="sidebar-form">
   						<div class="row">
   							<div class="col-sm-12">
   								<h3>New Procurement</h3>
   							</div>
   						</div>
   						<div class="row">
   							<div class="col-sm-6">
   								<p>Element</p>
   								<form class="navbar-form" role="search">
    								<div class="input-group add-on">
      									<input class="form-control" placeholder="WINDOWS" name="srch-term" id="srch-term" type="text">
      									<div class="input-group-btn">
        									<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      									</div>
    								</div>
  								</form>
   							</div>
   							<div class="col-sm-6">
   								<p>Location</p>
   								<input type="text" class="form-control" placeholder="Level 1" aria-label="start date" >
   							</div>
   						</div>

   						<div class="row">
   							<div class="col-sm-6">
   								<p>Supplier</p>
   								<form class="navbar-form" role="search">
    								<div class="input-group add-on">
      									<input class="form-control" placeholder="Indietech design age..." name="srch-term" id="srch-term" type="text">
      									<div class="input-group-btn">
        									<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      									</div>
    								</div>
  								</form>
   							</div>
   							<div class="col-sm-6">
   								<p>Supplier contact:</p>
			                	<div class="dropdown">
					  				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    			Ben Dexter <span class="caret"></span>
					  				</button>

					 				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    				<li><a href="#">Due Date</a></li>
					    				<li><a href="#">Another action</a></li>
					    				<li><a href="#">Something else here</a></li>
					  				</ul>
								</div>
   							</div>
   						</div>

   						<div class="row">
   							<div class="col-sm-6">
   								<p>Order No.</p>
   								<input type="text" class="form-control" placeholder="000152015-1" aria-label="Order no." >
   							</div>
   							<div class="col-sm-6">
   								
   							</div>
   						</div>

   						<div class="row">
   							<div class="col-sm-6">
   								<p>Start date:</p>
			                	<div class="dropdown">
					  				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    			_/_/__ <span class="caret"></span>
					  				</button>

					 				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    				<li><a href="#">Due Date</a></li>
					    				<li><a href="#">Another action</a></li>
					    				<li><a href="#">Something else here</a></li>
					  				</ul>
								</div>
   							</div>
   							<div class="col-sm-6">
   								<p>Target Completion:</p>
			                	<div class="dropdown">
					  				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    			_/_/__ <span class="caret"></span>
					  				</button>

					 				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    				<li><a href="#">Due Date</a></li>
					    				<li><a href="#">Another action</a></li>
					    				<li><a href="#">Something else here</a></li>
					  				</ul>
								</div>
   							</div>
   						</div>

   						<div class="row">
   							<div class="col-sm-12">
   								<p>Notes</p>
   								<textarea class="form-control" rows="5" id="notes"></textarea>
   							</div>
   						</div>

   						<div class="row">
   							<div class="col-sm-12">
   								<a class="save-button btn blue-btn" href=""><img src="img/save-icon.png" alt="">Save</a>
   							</div>
   						</div>
   					</div>
   				</div>
   				<div class="col-sm-7">
   					<div class="table-container">
   						<div class="row">
   							<div class="col-sm-12">
   								<p style="color:#a1a8ba;padding-left: 10px; ">WINDOWS</p>
   					<div id="accordion">
  						<div class="card">
    						<div class="card-header" id="headingOne">
      							<h5 class="mb-0">
        							<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          							<img src="img/arrow-down.png" alt=""> DESIGN
        							</button>
      							</h5>
    						</div>

    						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      							<div class="card-body">
       								<div class="table-responsive"> 
										<table class="table table-striped table-hover">
											<thead>
											    <tr>
											        <th></th>
											        <th></th>
											        <th>Supplier Contact</th>
											        <th>Started</th>
											        <th>Original</th>
											        <th>Actual</th>
											    </tr>
											</thead>
											<tbody>
											    <tr>
											        <td></td>
											        <td><img src="img/more-icon.png" alt="">Shop Drawing approval date</td>
											        <td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											        <td>__/__/____</td>
											        <td>__/__/____</td>
											        <td>          </td>
											    </tr>
											    <tr>
											        <td></td>
											        <td><img src="img/more-icon.png" alt="">Material checklist approval date</td>
											        <td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											        <td>__/__/____</td>
											        <td>__/__/____</td>
											        <td>          </td>
											    </tr>
											    <tr>
											        <td></td>
											        <td><img src="img/more-icon.png" alt="">Prototype unit</td>
											        <td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											        <td>__/__/____</td>
											        <td>__/__/____</td>
											        <td>          </td>
											    </tr>
											    <tr>
											        <td></td>
											        <td><img src="img/more-icon.png" alt="">Prototype approval deadline</td>
											        <td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											        <td>__/__/____</td>
											        <td>__/__/____</td>
											        <td>          </td>
											    </tr>
											</tbody>
										</table>
									</div>
      							</div>
    						</div>
    					</div>
    				<div class="col-sm-12 striped">
    					<p><img src="img/arrow-right.png" class="arrow-right">PRODUCTION</p>
    				</div>
    				<div class="col-sm-12">
    					<p><img src="img/arrow-right.png" class="arrow-right">GENERAL</p>
    				</div>
    				<div class="col-sm-12 striped">
    					<p><img src="img/arrow-right.png" class="arrow-right">VARIATION</p>
    				</div>
    				<div class="col-sm-12">
    					<p><img src="img/arrow-right.png" class="arrow-right">DELIVERY</p>
    				</div>
  				</div>
   				</div>
   				</div>
   				</div>
   			</div>
   			</div>
   		</div>
@endsection