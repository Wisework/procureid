<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
  			{
    		$role_customer = new Role();
    		$role_customer->name = 'customer';
		    $role_customer->save();

		    $role_supplier = new Role();
		    $role_supplier->name = 'supplier';
		    $role_supplier->save();

		    $role_manager = new Role();
		    $role_manager->name = 'manager';
		    $role_manager->save();

		    $role_companyadmin = new Role();
		    $role_companyadmin->name = 'companyadmin';
		    $role_companyadmin->save();
  			}
}
