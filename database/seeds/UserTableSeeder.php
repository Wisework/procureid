<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $role_customer = Role::where('name', 'customer')->first();
    $role_manager  = Role::where('name', 'manager')->first();
    $role_supplier = Role::where('name', 'supplier')->first();
    $role_companyadmin  = Role::where('name', 'companyadmin')->first();

    $customer = new User();
    $customer->firstname = 'Customer Name';
    $customer->lastname = 'Customer LastName';
    $customer->email = 'customer@example.com';
    $customer->phonenumber = '0011122223';
    $customer->password = bcrypt('secret');
    $customer->save();
    $customer->roles()->attach($role_customer);

    $manager = new User();
    $manager->firstname = 'Manager Name';
    $manager->lastname = 'Manager LastName';
    $manager->email = 'manager@example.com';
    $manager->phonenumber = '1144778899';
    $manager->password = bcrypt('secret');
    $manager->save();
    $manager->roles()->attach($role_manager);

    $supplier = new User();
    $supplier->firstname = 'Supplier Name';
    $supplier->lastname = 'Supplier LastName';
    $supplier->email = 'supplier@example.com';
    $supplier->phonenumber = '9988776655';
    $supplier->password = bcrypt('secret');
    $supplier->save();
    $supplier->roles()->attach($role_supplier);

    $companyadmin = new User();
    $companyadmin->firstname = 'Companyadmin Name';
    $companyadmin->lastname = 'Companyadmin LastName';
    $companyadmin->email = 'companyadmin@example.com';
    $companyadmin->phonenumber = '5588224466';
    $companyadmin->password = bcrypt('secret');
    $companyadmin->save();
    $companyadmin->roles()->attach($role_companyadmin);
    }
}
