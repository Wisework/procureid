@extends('layouts.app')

@section('content')
   		<div class="tasks-page">
   			<div class="row match-height">
   				<div class="col-sm-3">
   					<div class="sidebar">
   						<form class="navbar-form" role="search">
    						<div class="input-group add-on">
      							<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
      							<div class="input-group-btn">
        							<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      							</div>
    						</div>
  						</form>
  						<p>Filters</p>
  						<div class="filter">
  							<p>Location</p>
  							<div class="dropdown">
  								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    							Level 1 <span class="caret"></span>
  								</button>

 								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    								<li><a href="#">Due Date</a></li>
    								<li><a href="#">Another action</a></li>
    								<li><a href="#">Something else here</a></li>
  								</ul>
							</div>
  						</div>
  						<div class="filter">
  							<p>Filter by task status</p>
  							<div class="dropdown">
  								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    							All Active projects <span class="caret"></span>
  								</button>

 								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    								<li><a href="#">Due Date</a></li>
    								<li><a href="#">Another action</a></li>
    								<li><a href="#">Something else here</a></li>
  								</ul>
							</div>
  						</div>
  						<div class="filter">
  							<p>Filter by supplier</p>
  							<div class="dropdown">
  								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    							Window Design Agency <span class="caret"></span>
  								</button>

 								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    								<li><a href="#">Due Date</a></li>
    								<li><a href="#">Another action</a></li>
    								<li><a href="#">Something else here</a></li>
  								</ul>
							</div>
  						</div>
  						<div class="filter">
  							<p>Filter by element</p>
  							<div class="dropdown">
  								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    							Windows <span class="caret"></span>
  								</button>

 								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    								<li><a href="#">Due Date</a></li>
    								<li><a href="#">Another action</a></li>
    								<li><a href="#">Something else here</a></li>
  								</ul>
							</div>
  						</div>
   					</div>
   				</div>
   				<div class="col-sm-9">
   					<div class="row">
   						<div class="col-sm-12">
   							<div class="configue-options">
   								<img src="img/download-icon.png" alt="">
   							</div>
   						</div>
   					</div>
   					<div class="row">
   							<div class="col-sm-12">
   								<a href="" class="clear-filter">Clear Filter</a>
   							</div>
   						</div>
   					<div class="table-container">
   						<div class="row">
   							<div class="col-sm-7">
   								<p style="padding-left: 10px;">Filtered: Level 1 + Window Design Agency + Windows</p>
   							</div>
   							<div class="col-sm-5">
   								<div class="row">
   									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Unstarted</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Completed</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Overdue</p>
									</div>
   								</div>
   							</div>
   						</div>
   						<div class="row">
   							<div class="col-sm-12">
   								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th>Supplier Contact</th>
											<th>Started</th>
											<th>Original</th>
											<th>Actual</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><img src="img/box-icon.png" alt=""></td>
											<td><img src="img/more-icon.png" alt="">Shop Drawing approval date</td>
											<td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											<td>22/03/2016</td>
											<td>08/10/2017</td>
											<td>25/01/2018</td>
											<td>Complete <img src="img/overdue-icon.png" class="right"></td>
										</tr>
										<tr>
											<td><img src="img/box-icon.png" alt=""></td>
											<td><img src="img/more-icon.png" alt="">Material checklist approval date</td>
											<td><img src="img/eye-icon.png" alt="">Mark Wengritzky</td>
											<td>22/03/2016</td>
											<td>27/10/2017</td>
											<td></td>
											<td>Overdue <img src="img/overdue-icon.png" class="right"></td>
										</tr>
										<tr>
											<td><img src="img/box-icon.png" alt=""></td>
											<td><img src="img/more-icon.png" alt="">Prototype unit</td>
											<td><img src="img/eye-icon.png" alt="">Justin Williams</td>
											<td>22/03/2016</td>
											<td>01/12/2017</td>
											<td>25/11/2017</td>
											<td>Complete <img src="img/complete-icon.png" class="right"></td>
										</tr>
										<tr>
											<td><img src="img/box-icon.png" alt=""></td>
											<td><img src="img/more-icon.png" alt="">Prototype approval deadline</td>
											<td><img src="img/eye-icon.png" alt="">Justin Williams</td>
											<td>22/03/2016</td>
											<td>01/02/2018</td>
											<td></td>
											<td>Unstarted <img src="img/unstarted-icon.png" class="right"></td>
										</tr>
									</tbody>
								 </table>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
@endsection