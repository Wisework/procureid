@extends('layouts.app')

@section('content')
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="my-profile-heading">
   					<img src="/uploads/avatars/{{$user->avatar}}" alt="">
   					<h2>{{ $user->firstname}} {{ $user->lastname}}'s Profile</h2>
	   					<div class="avatar-upload">
		   					<form enctype="multipart/form-data" action="/my-profile" method="POST">
		   						{{ csrf_field() }}
		   						<label>Update Profile Image</label>
		   						<input type="file" name="avatar">
		   						<input type="submit" class="btn blue-btn">
		   					</form>
	   					</div>
   				</div>
   			</div>
   		</div>
   		<div class="row">
   			<div class="col-sm-12">
				<div class="profile-details">
					<h3>First Name: {{ $user->firstname}}</h3>
					<h3>Last Name: {{ $user->lastname}}</h3>
					<h3>Company: {{ $user->company}}</h3>
					<h3>Phone Number: {{ $user->phonenumber}}</h3>
					<h3>Email: {{ $user->email}}</h3>
				</div>
				<a href="/my-profile/{{$user->id}}" class="btn blue-btn">Edit Profile</a>
   			</div>
   		</div>
@endsection