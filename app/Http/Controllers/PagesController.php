<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $request->user()->authorizeRoles(['Customer', 'Supplier' , 'Procurement Manager' , 'Company Admin']);
        return view ('pages.index');
    }

    public function projects(Request $request){
        $request->user()->authorizeRoles(['Customer', 'Supplier' , 'Procurement Manager' , 'Company Admin']);
        return view ('pages.project');
    }

    public function tasks(Request $request){
        $request->user()->authorizeRoles(['Customer', 'Supplier' , 'Procurement Manager' , 'Company Admin']);
        return view ('pages.tasks');
    }

    public function system(Request $request){
        $request->user()->authorizeRoles(['Company Admin']);
        return view ('pages.system');
    }

    public function projectconfiguration(Request $request){
        $request->user()->authorizeRoles(['Customer', 'Supplier' , 'Procurement Manager' , 'Company Admin']);
        return view ('pages.proj-config');
    }
    public function addprocurement(Request $request){
        $request->user()->authorizeRoles(['Customer', 'Supplier' , 'Procurement Manager' , 'Company Admin']);
        return view ('pages.add-procurement');
    }
}
