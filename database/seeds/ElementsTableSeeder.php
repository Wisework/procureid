<?php

use Illuminate\Database\Seeder;

class ElementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = array(
            ['id' => 1, 
            'project_id' => 1, 
            'name' => 'Windows', 
            'description' => 'these are windows', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 2, 
            'project_id' => 1, 
            'name' => 'Joinery', 
            'description' => 'this is joinery', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 3, 
            'project_id' => 2, 
            'name' => 'Windows', 
            'description' => 'these are windows', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 4, 
            'project_id' => 1, 
            'name' => 'Windows', 
            'description' => 'these are windows', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
        );
        DB::table('elements')->insert($elements);
    }
}
