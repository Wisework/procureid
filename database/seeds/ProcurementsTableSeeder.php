<?php

use Illuminate\Database\Seeder;

class ProcurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $procurements = array(
            ['id' => 1, 
            'element_id' => 1, 
            'location_id' => 1, 
            'project_id' => 1, 
            'order_number' => 'No - 32456', 
            'start_date' => '2018/2/18', 
        	'target_completion' => '2018/7/18', 
        	'notes' => 'notes and such', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 2, 
            'element_id' => 2, 
            'location_id' => 1, 
            'project_id' => 1, 
            'order_number' => 'No - 32896', 
            'start_date' => '2018/2/18', 
        	'target_completion' => '2018/7/18', 
        	'notes' => 'notes and such', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 3, 
            'element_id' => 4, 
            'location_id' => 2, 
            'project_id' => 1, 
            'order_number' => 'No - 32896', 
            'start_date' => '2018/2/18', 
            'target_completion' => '2018/7/18', 
            'notes' => 'notes and such', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 4, 
            'element_id' => 3, 
            'location_id' => 3, 
            'project_id' => 2, 
            'order_number' => 'No - 21896', 
            'start_date' => '2018/2/18', 
        	'target_completion' => '2018/7/18', 
        	'notes' => 'notes and such', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
        );
        DB::table('procurements')->insert($procurements);
    }
}
