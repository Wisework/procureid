@extends('layouts.app')

@section('content')
	<div class="system-page">
   		<div class="row">
   			<div class="col-sm-12">
   				<div class="page-title">
   					<img src="img/system-icon.png" alt="">
   					<h3>System Configuration</h3>
   				</div>
   			</div>
   		</div>

   		<div class="row">
   			<div class="col-sm-2">
   				<div class="system-sidebar">
   					<div class="sidebar-area">
   						<img src="users-icon.png" alt="">
   						<h4>Users</h4>
   					</div>
   					<div class="sidebar-area">
   						<img src="template-icon.png" alt="">
   						<h4>Element Templates</h4>
   					</div>
   				</div>
   			</div>
   			<div class="col-sm-10">
   				<p>Users</p>
   				<div class="table-container">
   					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Name</th>
								<th>Company</th>
								<th>Email Address</th>
								<th>Phone</th>
								<th>User Role</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Ben Dexter</td>
								<td>Design Firm Pty Ltd</td>
								<td>bendexter@indietech.com.au</td>
								<td>+61 422 648 728</td>
								<td>Supplier</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Mark Wengritzky</td>
								<td>WiseWorking</td>
								<td>markw@wiseworking.com.au</td>
								<td>+61 422 555 555</td>
								<td>Procurement Manager</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Justin Williams</td>
								<td>WiseWorking</td>
								<td>justinw@wiseworking.com.au</td>
								<td>+61 422 666 666</td>
								<td>Procurement Manager</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
						</tbody>
					 </table>
   				</div>
   				<div class="user-buttons">
   					<a href="">View All Users</a>
   					<a href=""><span>+</span>Add User</a>
   				</div>
			<div class="row">
   				<div class="col-sm-4">
   					<p>Element Templates</p>
   					<div class="table-container">
   					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Template Name</th>
								<th>Description</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Balustrates</td>
								<td>Standard wooden balustrates</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Windows</td>
								<td>Standard window frames and windows</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Tiles</td>
								<td>Standard tiled areas</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Carpet</td>
								<td>Carpeting throughout office areas</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>Stairs</td>
								<td>Standard Exit Stairwell</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
						</tbody>
					 </table>
   				</div>
   			</div>
   				<div class="col-sm-8">
   					<p>Element Tasks</p>
   					<div class="table-container">
   					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Task Name</th>
								<th>Stage</th>
								<th>Value Type</th>
								<th></th>
								<th>QA-Image</th>
								<th>Dependency</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>On-site delivery</td>
								<td>Delivery</td>
								<td>Date</td>
								<td></td>
								<td><img src="img/box-icon.png" class="checkbox"></td>
								<td></td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Packaging & dispatch</td>
								<td>Production</td>
								<td>Formula</td>
								<td><span style="color: #4687c7;">+ 30 days</span></td>
								<td><img src="img/box-icon.png" class="checkbox"></td>
								<td>1</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Manufacturing</td>
								<td>Production</td>
								<td>Status</td>
								<td></td>
								<td><img src="img/box-icon.png" class="checkbox"></td>
								<td></td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>4</td>
								<td>Approval</td>
								<td>Design</td>
								<td>Formula</td>
								<td><span style="color: #4687c7;">+ 60 days</span></td>
								<td><img src="img/box-icon.png" class="checkbox"></td>
								<td>2</td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
							<tr>
								<td>5</td>
								<td>Production design</td>
								<td>Design</td>
								<td>Status</td>
								<td></td>
								<td><img src="img/box-icon.png" class="checkbox"></td>
								<td></td>
								<td><img src="img/more-icon.png" alt=""></td>
							</tr>
						</tbody>
					 </table>
					</div>
   				</div>
   			</div>
   			</div>
   		</div>
   	</div>
@endsection