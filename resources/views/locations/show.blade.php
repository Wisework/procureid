@extends('layouts.app')
@section('content')


<div class="project-page">
            
            <div id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><img src="{{URL::asset('/img/modal-icon.png')}}" alt=""> <span>ACTION TASK</span></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Shop Drawing approval date</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p>Element / Stage</p>
                                    <input type="text" class="form-control" placeholder="WINDOW / DESIGN" aria-label="Recipient's username" >
                                </div>
                                <div class="col-sm-4">
                                    <p>Supplier</p>
                                    <form class="navbar-form" role="search">
                                        <div class="input-group add-on">
                                            <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-4">
                                    <p>Supplier contact:</p>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Ben Dexter <span class="caret"></span>
                                        </button>

                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#">Due Date</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                            </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p>Start date:</p>
                                    <input type="text" class="form-control" placeholder="22/03/2016" aria-label="start date" >
                                </div>
                                <div class="col-sm-8">
                                    <p>Upload QA photo evidence</p>
                                    <input class="form-control half" type="text" class="form-control" placeholder="Filename.png" aria-label="Recipient's username" >
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p>Original due date:</p>
                                    <input type="text" class="form-control" placeholder="08/10/2017" aria-label="original due date" >
                                    <p>Set completion date:</p>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        25/01/2018 <span class="caret"></span>
                                        </button>

                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#">Due Date</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                            </ul>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <p>Add Notes</p>
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn cancel-btn" data-dismiss="modal">CANCEL</button>
                            <button type="button" class="btn previous-btn"><img src="{{URL::asset('/img/arrow-left.png')}}" alt="">Previous task</button>
                            <button type="button" class="btn next-btn">Next task <img src="{{URL::asset('/img/arrow-right.png')}}" alt=""></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row match-height">
                <div class="col-sm-2">
                    <div class="sidebar">
                        <form class="navbar-form" role="search">
                            <div class="input-group add-on">
                                <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>

                        @if ( !$projects->count() )
                            You have no projects
                        @else
                        <ul class="sidebar-project">
                            @foreach( $projects as $sidebar )
                                <li><a href="{{ route('projects.show', $sidebar->slug) }}"><img src="{{URL::asset('/img/sbar-project-blue.png')}}"><h3>{{ $sidebar->name }}</h3></a></li>
                            @endforeach

                        </ul>
                        @endif
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="single-project">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="project-name">
                                            <img src="{{URL::asset('/img/project-icon.png')}}" alt="">
                                            <h3>{{ $project->name }}</h3>
                                            <p>{{ $project->address1 }}, {{ $project->address2 }}, {{ $project->state }} {{ $project->postcode }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="pm-name">
                                            <p>PM: {{ $project->project_manager }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="project-controls">
                                            <p class="status">Status: <span class="{{ $project->status }}">{{ $project->status }}</span><img src="{{URL::asset('/img/overdue-icon.png')}}" alt=""></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-11 col-sm-offset-1">
                                        <div class="single-progress-bar">
                                        <p class="above-project progress-start"><span>Start:</span>{{ $project->start_date }}</p>
                                        <p class="above-project progress-original"><span>Original Est.:</span>{{ $project->original_estimate }}</p>
                                        <p class="above-project progress-current"><span>Current Est:</span>{{ $project->current_estimate }}</p>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" style="width: 35%">
                                                <span>45</span>
                                            </div>
                                            <div class="progress-bar progress-bar-warning" style="width: 55%">
                                                <span>65</span>
                                            </div>
                                            <div class="progress-bar progress-bar-danger" style="width: 10%">
                                                <span>12</span>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="configue-options">
                                <p><a href="{{ Request::url()}}/edit">Configue project<img src="{{URL::asset('/img/open-blue.png')}}" class="open-ico"> </a></p>
                                <p style="margin-top:60px;"><img src="{{URL::asset('/img/download-icon.png')}}" alt=""><img src="{{URL::asset('/img/clock-icon.png')}}" class="clock"><a class="btn blue-btn " href="{{ route('projects.create') }}">+ Add Project</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="row filters">
                        <div class="col-sm-3">
                            <div class="space">
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Actions <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Due Date</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <p>Location</p>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {{ $location->name }} <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        @foreach( $project->locations as $location )
                                        <li><a href="{{ route('projects.locations.show', [$project->slug , $location->id]) }}">{{ $location->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                        </div>
                        <div class="col-sm-3">
                           
                        </div>
                        <div class="col-sm-2">
                           
                        </div>
                        <div class="col-sm-2">
                            <div class="configue-options space">
                                <a class="btn blue-btn" href="{{ Request::url()}}/tasks/create">+ Add Procurement</a>
                            </div>
                        </div>
                    </div>
                
                    <div class="table-container">
                         @foreach( $elements as $element )

                        <div class="row table-headers">
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $element->id }}" aria-expanded="true" aria-controls="{{ $element->id }}">
                                                        <img src="{{URL::asset('/img/arrow-down.png')}}" alt="">{{ $element->name }}
                                                        </button>
                                                    </h5>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <p>Supplier: Window design agency</p>
                                    </div>
                                    <div class="col-sm-4">
                                        @foreach ($procurements as $procurement)
                                        @if( $element->id == $procurement->element_id)
                                            
                                                <p>{{ $procurement->order_number}}</p>
                                            
                                        @else
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p class="task-total"><img src="" alt=""> Unstarted</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p class="task-total"><img src="" alt=""> Completed</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <p class="task-total"><img src="" alt=""> Overdue</p>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-12">
                                <div id="{{ $element->id }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">

                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $element->id }}2" aria-expanded="true" aria-controls="{{ $element->id }}2">
                                                     <img src="{{URL::asset('/img/arrow-down.png')}}" alt=""> Design
                                                    </button>
                                                    </h5>
                                                </div>

                                        <div id="{{ $element->id }}2" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="table-responsive"> 
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th>Supplier Contact</th>
                                                            <th>Started</th>
                                                            <th>Original</th>
                                                            <th>Actual</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ( !$tasks->count() )
                                                            Your project has no tasks.
                                                        @else
                                                         @foreach( $tasks as $task )
                                                         @if ($task->catergory == 'Design')
                                                         @foreach( $procurements as $proc )
                                                            @if ($proc->id == $task->procurement_id and $element->id == $proc->element_id)
                                                        <tr class='clickable-row' data-href="{{ route('procurements.tasks.edit', [ $proc->id , $task->slug]) }}" role="button" >

                                                            <td><img src="{{URL::asset('/img/box-icon.png')}}" class="checkbox"></td>
                                                            <td><img src="{{URL::asset('/img/more-icon.png')}}" alt="">{{ $task->name }}</td>
                                                            <td><img src="{{URL::asset('/img/eye-icon.png')}}" alt="">{{ $task->supplier_contact }}</td>
                                                            <td>{{ $task->start_date }}</td>
                                                            <td>{{ $task->original_estimate }}</td>
                                                            <td>{{ $task->actual_date }}</td>
                                                            <td>{{ $task->status }} <img src="{{URL::asset('/img/overdue-icon.png')}}" class="right"></td>
                                                            <td><a href="{{ route('procurements.tasks.create', $proc->id ) }}" class="btn blue-btn"><span>+</span>Create Task</a></td>
                                                             <td><form class="confirm inline-form" action="{{ route('procurements.tasks.destroy', [$proc->id, $task->slug]) }}" method="post">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE') }}
                                                                <div class="form-group">
                                                                    <button type="submit"><img src="{{URL::asset('/img/delete-icon.png')}}"></button>
                                                                </div>
                                                            </form>
                                                            </td>
                                                        </tr>
                                                            @else
                                                            @endif
                                                        @endforeach
                                                        @endif
                                                        @endforeach
                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">

                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $element->id }}3" aria-expanded="true" aria-controls="{{ $element->id }}3">
                                                     <img src="{{URL::asset('/img/arrow-down.png')}}" alt=""> Production
                                                    </button>
                                                    </h5>
                                                </div>

                                        <div id="{{ $element->id }}3" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="table-responsive"> 
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th>Supplier Contact</th>
                                                            <th>Started</th>
                                                            <th>Original</th>
                                                            <th>Actual</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ( !$tasks->count() )
                                                            Your project has no tasks.
                                                        @else
                                                         @foreach( $tasks as $task )
                                                         @if ($task->catergory == 'Production')
                                                         @foreach( $procurements as $proc )
                                                            @if ($proc->id == $task->procurement_id and $element->id == $proc->element_id)
                                                       <tr class='clickable-row' data-href="{{ route('procurements.tasks.edit', [ $proc->id , $task->slug]) }}" role="button" >

                                                            <td><img src="{{URL::asset('/img/box-icon.png')}}" class="checkbox"></td>
                                                            <td><img src="{{URL::asset('/img/more-icon.png')}}" alt="">{{ $task->name }}</td>
                                                            <td><img src="{{URL::asset('/img/eye-icon.png')}}" alt="">{{ $task->supplier_contact }}</td>
                                                            <td>{{ $task->start_date }}</td>
                                                            <td>{{ $task->original_estimate }}</td>
                                                            <td>{{ $task->actual_date }}</td>
                                                            <td>{{ $task->status }} <img src="{{URL::asset('/img/overdue-icon.png')}}" class="right"></td>
                                                            <td><a href="{{ route('procurements.tasks.create', $proc->id ) }}" class="btn blue-btn"><span>+</span>Create Task</a></td>
                                                             <td><form class="confirm inline-form" action="{{ route('procurements.tasks.destroy', [$proc->id, $task->slug]) }}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                             <div class="form-group">
                                                            <button type="submit"><img src="{{URL::asset('/img/delete-icon.png')}}"></button>
                                                            </div>
                                                            </form>
                                                            </td>
                                                        </tr>
                                                            @else
                                                            @endif
                                                        @endforeach
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">

                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $element->id }}4" aria-expanded="true" aria-controls="{{ $element->id }}4">
                                                     <img src="{{URL::asset('/img/arrow-down.png')}}" alt=""> Shipping
                                                    </button>
                                                    </h5>
                                                </div>

                                        <div id="{{ $element->id }}4" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="table-responsive"> 
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th>Supplier Contact</th>
                                                            <th>Started</th>
                                                            <th>Original</th>
                                                            <th>Actual</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ( !$tasks->count() )
                                                            Your project has no tasks.
                                                        @else
                                                         @foreach( $tasks as $task )
                                                         @if ($task->catergory == 'Shipping')
                                                         @foreach( $procurements as $proc )
                                                            @if ($proc->id == $task->procurement_id and $element->id == $proc->element_id)
                                                         <tr class='clickable-row' data-href="{{ route('procurements.tasks.edit', [ $proc->id , $task->slug]) }}" role="button" >

                                                            <td><img src="{{URL::asset('/img/box-icon.png')}}" class="checkbox"></td>
                                                            <td><img src="{{URL::asset('/img/more-icon.png')}}" alt="">{{ $task->name }}</td>
                                                            <td><img src="{{URL::asset('/img/eye-icon.png')}}" alt="">{{ $task->supplier_contact }}</td>
                                                            <td>{{ $task->start_date }}</td>
                                                            <td>{{ $task->original_estimate }}</td>
                                                            <td>{{ $task->actual_date }}</td>
                                                            <td>{{ $task->status }} <img src="{{URL::asset('/img/overdue-icon.png')}}" class="right"></td>
                                                            <td><a href="{{ route('procurements.tasks.create', $proc->id ) }}" class="btn blue-btn"><span>+</span>Create Task</a></td>
                                                            <td><form class="confirm inline-form" action="{{ route('procurements.tasks.destroy', [$proc->id, $task->slug]) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <button type="submit"><img src="{{URL::asset('/img/delete-icon.png')}}"></button>
                                        </div>
                                    </form>
                                    </td>
                                                        </tr>
                                                            @else
                                                            @endif
                                                        @endforeach
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>

                             </div>
                        </div>
                    </div><!-- row -->

                    @endforeach
                    
                    
                </div>
            </div>
        </div> 
@endsection