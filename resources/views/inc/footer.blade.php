<div class="footer-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="app-logo">
					<img src="{{URL::asset('/img/app-logo.png')}}" alt="">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="support">
					<a href="">Click here to get support <img src="{{URL::asset('/img/support.png')}}" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>