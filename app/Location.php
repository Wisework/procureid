<?php

namespace App;

use App\Project;
use App\Task;
use App\Element;
use App\Procurement;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $table = 'locations';

    protected $guarded = [];

    public function project()
	{
		return $this->belongsTo(Project::class);
	}
	public function procurements()
	{
		return $this->hasMany(Procurement::class, 'location_id' , 'id');
	}
}
