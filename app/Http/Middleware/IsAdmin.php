<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->authorizeRoles(['Company Admin'])) {

        return $next($request);
        }
    }
}
