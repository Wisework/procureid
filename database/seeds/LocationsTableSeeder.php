<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = array(
            ['id' => 1, 
            'project_id' => 1, 
            'name' => 'Level 1', 
            'slug' => 'level-1', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 2, 
            'project_id' => 1, 
            'name' => 'Level 2', 
            'slug' => 'level-2', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
            ['id' => 3, 
            'project_id' => 2, 
            'name' => 'Level 1', 
            'slug' => 'level-1', 
            'created_at' => new DateTime, 
            'updated_at' => new DateTime],
        );
        DB::table('locations')->insert($locations);
    }
}
