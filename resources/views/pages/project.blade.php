@extends('layouts.app')

@section('content')
   		<div class="project-page">
   			
 
			<div id="myModal" class="modal fade">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title"><img src="img/modal-icon.png" alt=""> <span>ACTION TASK</span></h4>
			            </div>
			            <div class="modal-body">
			                <div class="row">
			                	<div class="col-sm-12">
			                		<h3>Shop Drawing approval date</h3>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-4">
			                		<p>Element / Stage</p>
			                		<input type="text" class="form-control" placeholder="WINDOW / DESIGN" aria-label="Recipient's username" >
			                	</div>
			                	<div class="col-sm-4">
			                		<p>Supplier</p>
			                		<form class="navbar-form" role="search">
					    				<div class="input-group add-on">
					      					<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
					      					<div class="input-group-btn">
					        					<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					      					</div>
					    				</div>
					  				</form>
			                	</div>
			                	<div class="col-sm-4">
			                		<p>Supplier contact:</p>
			                		<div class="dropdown">
					  					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    				Ben Dexter <span class="caret"></span>
					  					</button>

					 						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    						<li><a href="#">Due Date</a></li>
					    						<li><a href="#">Another action</a></li>
					    						<li><a href="#">Something else here</a></li>
					  						</ul>
									</div>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-4">
			                		<p>Start date:</p>
			                		<input type="text" class="form-control" placeholder="22/03/2016" aria-label="start date" >
			                	</div>
			                	<div class="col-sm-8">
			                		<p>Upload QA photo evidence</p>
			                		<input class="form-control half" type="text" class="form-control" placeholder="Filename.png" aria-label="Recipient's username" >
			                	</div>

			                </div>
			                <div class="row">
			                	<div class="col-sm-4">
			                		<p>Original due date:</p>
			                		<input type="text" class="form-control" placeholder="08/10/2017" aria-label="original due date" >
			                		<p>Set completion date:</p>
			                		<div class="dropdown">
					  					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    				25/01/2018 <span class="caret"></span>
					  					</button>

					 						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    						<li><a href="#">Due Date</a></li>
					    						<li><a href="#">Another action</a></li>
					    						<li><a href="#">Something else here</a></li>
					  						</ul>
									</div>
			                	</div>
			                	<div class="col-sm-8">
			                		<p>Add Notes</p>
			                		<textarea class="form-control" rows="5" id="comment"></textarea>
			                	</div>
			                </div>
			            </div>
			            <div class="modal-footer">
			                <button type="button" class="btn cancel-btn" data-dismiss="modal">CANCEL</button>
			                <button type="button" class="btn previous-btn"><img src="img/arrow-left.png" alt="">Previous task</button>
			                <button type="button" class="btn next-btn">Next task <img src="img/arrow-right.png" alt=""></button>
			            </div>
			        </div>
			    </div>
			</div>

   			<div class="row match-height">
   				<div class="col-sm-2">
   					<div class="sidebar">
   						<form class="navbar-form" role="search">
    						<div class="input-group add-on">
      							<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
      							<div class="input-group-btn">
        							<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      							</div>
    						</div>
  						</form>

  						<div class="sidebar-project blue">
  							<div class="triangle"></div>
  							<img src="img/sbar-project-white.png" alt=""> <h3>Parkdale Heights Co...</h3>
  						</div>
  						<div class="sidebar-project">
  							<img src="img/sbar-project-blue.png" alt=""> <h3>Domain Park Highrise</h3>
  						</div>
  						<div class="sidebar-project">
  							<img src="img/sbar-project-blue.png" alt=""> <h3>Melbourne Airport</h3>
  						</div>
  						<div class="sidebar-project">
  							<img src="img/sbar-project-blue.png" alt=""> <h3>Docklands Carpark</h3>
  						</div>
  						<div class="sidebar-project">
  							<img src="img/sbar-project-blue.png" alt=""> <h3>Sydney Towers</h3>
  						</div>
   					</div>
   				</div>
   				<div class="col-sm-10">
   					<div class="row">
   						<div class="col-sm-9">
   							<div class="single-project">

   								<div class="row">
   									<div class="col-sm-6">
   										<div class="project-name">
   											<img src="img/project-icon.png" alt="">
   											<h3>Parkdale Heights Complex</h3>
   											<p>13 Smith Street, Parkdale Heights VIC</p>
   										</div>
   									</div>
   									<div class="col-sm-3">
   										<div class="pm-name">
   											<p>PM: Justin Williams</p>
   										</div>
   									</div>
   									<div class="col-sm-3">
   										<div class="project-controls">
   											<p class="status">Status: <span>Overdue</span><img src="img/overdue-icon.png" alt=""></p>
   										</div>
   									</div>
   								</div>

   								<div class="row">
   									<div class="col-sm-11 col-sm-offset-1">
   										<div class="single-progress-bar">
   										<p class="above-project progress-start"><span>Start:</span>10 Apr 2017</p>
   										<p class="above-project progress-original"><span>Original Est.:</span>10 Apr 2017</p>
   										<p class="above-project progress-current"><span>Current Est:</span>10 Apr 2017</p>
   										<div class="progress">
  											<div class="progress-bar progress-bar-success" style="width: 35%">
    											<span>45</span>
  											</div>
  											<div class="progress-bar progress-bar-warning" style="width: 55%">
    											<span>65</span>
  											</div>
  											<div class="progress-bar progress-bar-danger" style="width: 10%">
    											<span>12</span>
  											</div>
										</div>
										</div>
   									</div>
   								</div>

   							</div>
   						</div>
   						<div class="col-sm-3">
   							<div class="configue-options">
   								<p>Configue project<img src="img/open-blue.png" class="open-ico"> </p>
   								<p style="margin-top:60px;"><img src="img/download-icon.png" alt=""><img src="img/clock-icon.png" class="clock"><a class="btn blue-btn "href="">+ Add Project</a></p>
   							</div>
   						</div>
   					</div>

   					<div class="row filters">
   						<div class="col-sm-3">
   							<div class="space">
								<div class="dropdown">
  									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    								Actions <span class="caret"></span>
  									</button>

 									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    									<li><a href="#">Due Date</a></li>
    									<li><a href="#">Another action</a></li>
    									<li><a href="#">Something else here</a></li>
  									</ul>
								</div>
							</div>
   						</div>
   						<div class="col-sm-2">
   							<p>Location</p>
								<div class="dropdown">
  									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    								Level 1 <span class="caret"></span>
  									</button>

 									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    									<li><a href="#">Due Date</a></li>
    									<li><a href="#">Another action</a></li>
    									<li><a href="#">Something else here</a></li>
  									</ul>
								</div>
   						</div>
   						<div class="col-sm-3">
   							<p>Filter by task status</p>
								<div class="dropdown">
  									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    								All active tasks <span class="caret"></span>
  									</button>

 									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    									<li><a href="#">Due Date</a></li>
    									<li><a href="#">Another action</a></li>
    									<li><a href="#">Something else here</a></li>
  									</ul>
								</div>
   						</div>
   						<div class="col-sm-2">
   							<p>Filter by supplier</p>
								<div class="dropdown">
  									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    								All suppliers <span class="caret"></span>
  									</button>

 									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    									<li><a href="#">Due Date</a></li>
    									<li><a href="#">Another action</a></li>
    									<li><a href="#">Something else here</a></li>
  									</ul>
								</div>
   						</div>
   						<div class="col-sm-2">
   							<div class="configue-options space">
   								<a class="btn blue-btn" href="/add-procurement">+ Add Procurement</a>
   							</div>
   						</div>
   					</div>
				
					<div class="table-container">
						<div class="row table-headers">
							<div class="col-sm-7">
								<div class="row">
									<div class="col-sm-4">
										<p><img src="img/arrow-down.png" alt=""><a href="">WINDOWS</a> <img src="img/more-icon.png" alt=""></p>
									</div>
									<div class="col-sm-4">
										<p>Supplier: Window design agency</p>
									</div>
									<div class="col-sm-4">
										<p>Order No. 0568-1</p>
									</div>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Unstarted</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Completed</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Overdue</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div id="accordion">
  									<div class="card">
    									<div class="card-header" id="headingOne">
      										<h5 class="mb-0">
        										<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          										<img src="img/arrow-down.png" alt=""> DESIGN
        										</button>
      										</h5>
    									</div>

    								<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      									<div class="card-body">
       										<div class="table-responsive"> 
											    <table class="table table-striped table-hover">
											        <thead>
											            <tr>
											            	<th></th>
											                <th></th>
											                <th>Supplier Contact</th>
											                <th>Started</th>
											                <th>Original</th>
											                <th>Actual</th>
											                <th></th>
											            </tr>
											        </thead>
											        <tbody>
											            <tr data-toggle="modal" href="#myModal" role="button" >
											            	<td><img src="img/box-icon.png" class="checkbox"></td>
											                <td><img src="img/more-icon.png" alt="">Shop Drawing approval date</td>
											                <td><img src="img/eye-icon.png" alt="">Ben Dexter</td>
											                <td>22/03/2016</td>
											                <td>08/10/2017</td>
											                <td>25/01/2018</td>
											                <td>Complete <img src="img/overdue-icon.png" class="right"></td>
											            </tr>
											            <tr>
											            	<td><img src="img/box-icon.png" class="checkbox"></td>
											                <td><img src="img/more-icon.png" alt="">Material checklist approval date</td>
											                <td><img src="img/eye-icon.png" alt="">Mark Wengritzky</td>
											                <td>22/03/2016</td>
											                <td>27/10/2017</td>
											                <td></td>
											                <td>Overdue <img src="img/overdue-icon.png" class="right"></td>
											            </tr>
											            <tr>
											            	<td><img src="img/box-icon.png" class="checkbox"></td>
											                <td><img src="img/more-icon.png" alt="">Prototype unit</td>
											                <td><img src="img/eye-icon.png" alt="">Justin Williams</td>
											                <td>22/03/2016</td>
											                <td>01/12/2017</td>
											                <td>25/11/2017</td>
											                <td>Complete <img src="img/complete-icon.png" class="right"></td>
											            </tr>
											            <tr>
											            	<td><img src="img/box-icon.png" class="checkbox"></td>
											                <td><img src="img/more-icon.png" alt="">Prototype approval deadline</td>
											                <td><img src="img/eye-icon.png" alt="">Justin Williams</td>
											                <td>22/03/2016</td>
											                <td>01/02/2018</td>
											                <td></td>
											                <td>Unstarted <img src="img/unstarted-icon.png" class="right"></td>
											            </tr>
											        </tbody>
											    </table>
											</div>
      									</div>
    								</div>
    								</div>
    								<div class="col-sm-12 striped">
    									<p><img src="img/arrow-right.png" class="arrow-right">PRODUCTION</p>
    								</div>
    								<div class="col-sm-12">
    									<p><img src="img/arrow-right.png" class="arrow-right">GENERAL</p>
    								</div>
    								<div class="col-sm-12 striped">
    									<p><img src="img/arrow-right.png" class="arrow-right">VARIATION</p>
    								</div>
    								<div class="col-sm-12">
    									<p><img src="img/arrow-right.png" class="arrow-right">DELIVERY</p>
    								</div>
  								</div>
							</div>
						</div>
					</div>
					
					<div class="table-container">
						<div class="row">
							<div class="col-sm-7">
								<p><img src="img/arrow-right.png" class="arrow-right">Elevators <img src="" alt=""></p>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Unstarted</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Completed</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Overdue</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="table-container">
						<div class="row">
							<div class="col-sm-7">
								<p><img src="img/arrow-right.png" class="arrow-right">Form Work <img src="" alt=""></p>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Unstarted</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Completed</p>
									</div>
									<div class="col-sm-4">
										<p class="task-total"><img src="" alt=""> Overdue</p>
									</div>
								</div>
							</div>
						</div>
					</div>
   				</div>
   			</div>
   		</div> 
@endsection