<?php

namespace App;

use App\Project;
use App\Task;
use App\Location;
use App\Procurement;
use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
	protected $table = 'elements';

    protected $guarded = [];

    public function project()
	{
		return $this->belongsTo(Project::class);
	}

	public function procurements()
	{
		return $this->hasMany(Procurement::class, 'element_id' , 'id');
	}
}
